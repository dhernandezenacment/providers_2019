// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaOrgTipoCreate from '../../../../../assets/schemas/organizacionTipo/OrganizacionTipoCreate.json';
import * as schemaOrgTipoDelete from '../../../../../assets/schemas/organizacionTipo/OrganizacionTipoDelete.json';
import * as schemaOrgTipoGet from '../../../../../assets/schemas/organizacionTipo/OrganizacionTipoGet.json';
import * as schemaOrgTipoLista from '../../../../../assets/schemas/organizacionTipo/OrganizacionTipoLista.json';
import * as schemaOrgTipoUpdate from '../../../../../assets/schemas/organizacionTipo/OrganizacionTipoUpdate.json';

@Component({
	selector: 'organizacionTipo',
	templateUrl: './organizacionTipo.component.html',
	styleUrls: ['./organizacionTipo.component.scss']
})
export class OrganizacionTipoComponent implements OnInit {
	jsonOrgTipoCreate: object = schemaOrgTipoCreate;
	jsonOrgTipoDelete: object = schemaOrgTipoDelete;
	jsonOrgTipoGet: object = schemaOrgTipoGet;
	jsonOrgTipoLista: object = schemaOrgTipoLista;
	jsonOrgTipoUpdate: object = schemaOrgTipoUpdate;

	public createForm: FormGroup;
	public deleteForm: FormGroup;
	public getForm: FormGroup;
	public updateForm: FormGroup;
	constructor(private ProvRequest: ProvService) { }

	ngOnInit() {
		console.log('Organizacion Tipo');
		this.createForm = new FormGroup({
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.updateForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
