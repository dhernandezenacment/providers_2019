// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaServicioCreate from '../../../../../assets/schemas/servicio/ServicioCreate.json';
import * as schemaServicioDelete from '../../../../../assets/schemas/servicio/ServicioDelete.json';
import * as schemaServicioGet from '../../../../../assets/schemas/servicio/ServicioGet.json';
import * as schemaServicioLista from '../../../../../assets/schemas/servicio/ServicioLista.json';
import * as schemaServicioUpdate from '../../../../../assets/schemas/servicio/ServicioUpdate.json';

@Component({
	selector: 'servicio',
	templateUrl: './servicio.component.html',
	styleUrls: ['./servicio.component.scss']
})
export class ServicioComponent implements OnInit {
	jsonServicioCreate: object = schemaServicioCreate;
	jsonServicioDelete: object = schemaServicioDelete;
	jsonServicioGet: object = schemaServicioGet;
	jsonServicioLista: object = schemaServicioLista;
	jsonServicioUpdate: object = schemaServicioUpdate;

	public listForm: FormGroup;
	public createForm: FormGroup;
	public deleteForm: FormGroup;
	public getForm: FormGroup;
	public updateForm: FormGroup;
	constructor(private ProvRequest: ProvService) { }

	ngOnInit() {
		console.log('Servicio');
		this.listForm = new FormGroup({
			activo: new FormControl(''),
			page: new FormControl(''),
			pageSize: new FormControl(''),
			name: new FormControl(''),
			tipo_servicio_id: new FormControl('')
		});
		this.createForm = new FormGroup({
			tipo_servicio_id: new FormControl('', [Validators.required]),
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.updateForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
			tipo_servicio_id: new FormControl('', [Validators.required]),
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
