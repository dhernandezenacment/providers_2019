// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaCuentaLista from '../../../../../assets/schemas/cuenta/CuentaLista.json';
import * as schemaCuentaGet from '../../../../../assets/schemas/cuenta/CuentaGet.json';
import * as schemaCuentaCreate from '../../../../../assets/schemas/cuenta/CuentaCreate.json';
import * as schemaCuentaUpdate from '../../../../../assets/schemas/cuenta/CuentaUpdate.json';
import * as schemaCuentaDelete from '../../../../../assets/schemas/cuenta/CuentaDelete.json';

@Component({
	selector: 'cuenta',
	templateUrl: './cuenta.component.html',
	styleUrls: ['./cuenta.component.scss']
})
export class CuentaComponent implements OnInit {
	jsonCuentaLista: object = schemaCuentaLista;
	jsonCuentaGet: object = schemaCuentaGet;
	jsonCuentaCreate: object = schemaCuentaCreate;
	jsonCuentaUpdate: object = schemaCuentaUpdate;
	jsonCuentaDelete: object = schemaCuentaDelete;

	public listForm: FormGroup;
	public getForm: FormGroup;
	public createForm: FormGroup;
	public updateForm: FormGroup;
	public deleteForm: FormGroup;
	constructor(private ProvRequest: ProvService) { }

	ngOnInit() {
		console.log('Cuenta');
		this.listForm = new FormGroup({
			activo: new FormControl(''),
			page: new FormControl(''),
			pageSize: new FormControl(''),
			name: new FormControl('', [Validators.maxLength(60)])
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.createForm = new FormGroup({
			banco_id: new FormControl('', [Validators.required]),
			estado_id: new FormControl('', [Validators.required]),
			organizacion_id: new FormControl('', [Validators.required]),
			razon_social: new FormControl('', [Validators.required,Validators.maxLength(45)]),
			causa_retencion: new FormControl(''),
			created_at: new FormControl(''),
			updated_at: new FormControl(''),
			con_factura: new FormControl(''),
			tarjetahabiente: new FormControl('', [Validators.maxLength(100)]),
			sucursal_bancaria: new FormControl('', [Validators.maxLength(255)]),
			cuenta: new FormControl('', [Validators.maxLength(100)]),
			pago_directo: new FormControl(''),
			pago_urgente: new FormControl(''),
			clabe: new FormControl('', [Validators.maxLength(18)]),
			activo: new FormControl(true)
		});
		this.updateForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
			banco_id: new FormControl('', [Validators.required]),
			estado_id: new FormControl('', [Validators.required]),
			organizacion_id: new FormControl('', [Validators.required]),
			razon_social: new FormControl('', [Validators.required,Validators.maxLength(45)]),
			causa_retencion: new FormControl(''),
			created_at: new FormControl(''),
			updated_at: new FormControl(''),
			con_factura: new FormControl(''),
			tarjetahabiente: new FormControl('', [Validators.maxLength(100)]),
			sucursal_bancaria: new FormControl('', [Validators.maxLength(255)]),
			cuenta: new FormControl('', [Validators.maxLength(100)]),
			pago_directo: new FormControl(''),
			pago_urgente: new FormControl(''),
			clabe: new FormControl('', [Validators.maxLength(18)]),
			activo: new FormControl(true)
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
