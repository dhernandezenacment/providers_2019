// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaOrgCreate from '../../../../../assets/schemas/organizacion/OrganizacionCreate.json';
import * as schemaOrgDelete from '../../../../../assets/schemas/organizacion/OrganizacionDelete.json';
import * as schemaOrgGet from '../../../../../assets/schemas/organizacion/OrganizacionGet.json';
import * as schemaOrgLista from '../../../../../assets/schemas/organizacion/OrganizacionLista.json';
import * as schemaOrgUpdate from '../../../../../assets/schemas/organizacion/OrganizacionUpdate.json';

@Component({
	selector: 'organizacion',
	templateUrl: './organizacion.component.html',
	styleUrls: ['./organizacion.component.scss']
})
export class OrganizacionComponent implements OnInit {
	jsonOrgCreate: object = schemaOrgCreate;
	jsonOrgDelete: object = schemaOrgDelete;
	jsonOrgGet: object = schemaOrgGet;
	jsonOrgLista: object = schemaOrgLista;
	jsonOrgUpdate: object = schemaOrgUpdate;

	public listForm: FormGroup;
	public createForm: FormGroup;
	public deleteForm: FormGroup;
	public getForm: FormGroup;
	public updateForm: FormGroup;

	public paramsId;
	public paramsOption;
		// list
		// details
		// update
		// delete
		// create

	constructor(private ProvRequest: ProvService, private rutaActiva: ActivatedRoute, private router: Router) {
	}

	ngOnInit() {
		this.rutaActiva.params.subscribe(
			(params: Params) => {
			  this.paramsId = params.id;
			  this.paramsOption = params.option;
			}
		  );
		console.log('Organizacion');
		console.log(this.paramsId);
		console.log(this.paramsOption);
		if (this.paramsOption !== 'list' && this.paramsOption !== 'details' && this.paramsOption !== 'update' && this.paramsOption !== 'create' && this.paramsOption !== 'delete') {
			this.router.navigate(['/organization/list']);
			this.paramsOption = 'list';
		}
		this.listForm = new FormGroup({
			rfc: new FormControl(''),
			curp: new FormControl(''),
			organizacion_id: new FormControl(''),
			page: new FormControl(''),
			pageSize: new FormControl(''),
			tipo_organizacion_id: new FormControl(''),
			correo_electronico: new FormControl(''),
			nombre_comercial: new FormControl(''),
			activo: new FormControl('')
		});
		this.createForm = new FormGroup({
			// organizacion_id: new FormControl(''),
			tipo_organizacion_id: new FormControl('', [Validators.required]),
			correo_electronico: new FormControl('', [Validators.required]),
			nombre_comercial: new FormControl('', [Validators.required]),
			comentario: new FormControl(''),
			rfc: new FormControl(''),
			curp: new FormControl(''),
			activo: new FormControl(true)
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.updateForm = new FormGroup({
			// organizacion_id: new FormControl(null),
			tipo_organizacion_id: new FormControl(''),
			correo_electronico: new FormControl('', [Validators.required]),
			nombre_comercial: new FormControl('', [Validators.required]),
			comentario: new FormControl(''),
			rfc: new FormControl(''),
			curp: new FormControl(''),
			activo: new FormControl(true, [Validators.required]),
			id: new FormControl('', [Validators.required]),
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		console.log(formValue);
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
