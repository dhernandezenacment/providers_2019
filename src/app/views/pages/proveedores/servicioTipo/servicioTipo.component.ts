// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaServicioTipoCreate from '../../../../../assets/schemas/servicioTipo/ServicioTipoCreate.json';
import * as schemaServicioTipoDelete from '../../../../../assets/schemas/servicioTipo/ServicioTipoDelete.json';
import * as schemaServicioTipoGet from '../../../../../assets/schemas/servicioTipo/ServicioTipoGet.json';
import * as schemaServicioTipoLista from '../../../../../assets/schemas/servicioTipo/ServicioTipoLista.json';
import * as schemaServicioTipoUpdate from '../../../../../assets/schemas/servicioTipo/ServicioTipoUpdate.json';

@Component({
	selector: 'servicioTipo',
	templateUrl: './servicioTipo.component.html',
	styleUrls: ['./servicioTipo.component.scss']
})
export class ServicioTipoComponent implements OnInit {
	jsonServicioTipoCreate: object = schemaServicioTipoCreate;
	jsonServicioTipoDelete: object = schemaServicioTipoDelete;
	jsonServicioTipoGet: object = schemaServicioTipoGet;
	jsonServicioTipoLista: object = schemaServicioTipoLista;
	jsonServicioTipoUpdate: object = schemaServicioTipoUpdate;

	public createForm: FormGroup;
	public deleteForm: FormGroup;
	public getForm: FormGroup;
	public updateForm: FormGroup;
	constructor(private ProvRequest: ProvService) { }

	ngOnInit() {
		console.log('Servicio Tipo');
		this.createForm = new FormGroup({
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.updateForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
			nombre: new FormControl('', [Validators.required,Validators.maxLength(100)]),
			activo: new FormControl(true, [Validators.required])
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
