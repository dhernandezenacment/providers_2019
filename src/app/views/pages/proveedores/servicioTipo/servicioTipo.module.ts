// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// AngularMaterial
import {
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatInputModule,
	MatFormFieldModule
} from '@angular/material';
// NgBootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Partials
import { PartialsModule } from '../../../partials/partials.module';
// Pages
import { CoreModule } from '../../../../core/core.module';
import { ServicioTipoComponent } from './servicioTipo.component';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';

@NgModule({
	exports: [],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		NgbModule,
		CoreModule,
		PartialsModule,
		RouterModule.forChild([
			{
				path: '',
				component: ServicioTipoComponent
			},
		]),
		MatButtonModule,
		MatCardModule,
		MatCheckboxModule,
		MatInputModule,
		MatFormFieldModule
	],
	providers: [
		ProvService
	],
	declarations: [
		ServicioTipoComponent
	]
})
export class ServicioTipoModule {
}
