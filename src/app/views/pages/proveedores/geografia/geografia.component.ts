// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaEstadoLista from '../../../../../assets/schemas/geografia/EstadoLista.json';
import * as schemaMunicipioLista from '../../../../../assets/schemas/geografia/MunicipioLista.json';
import * as schemaCPLista from '../../../../../assets/schemas/geografia/CPLista.json';

@Component({
	selector: 'geografia',
	templateUrl: './geografia.component.html',
	styleUrls: ['./geografia.component.scss']
})
export class GeografiaComponent implements OnInit {
	jsonEstadoLista: object = schemaEstadoLista;
	jsonMunicipioLista: object = schemaMunicipioLista;
	jsonCPLista: object = schemaCPLista;

	public getMpForm: FormGroup;
	public getCPForm: FormGroup;
	constructor(private ProvRequest: ProvService) { }

	ngOnInit() {
		console.log('Geografia');
		this.getMpForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.getCPForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		console.log('onSubmitGet');
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}
}
