// Angular
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
//Services
import { ProvService } from '../../../../core/_base/layout/services/provRequests.servicio';
//Schemas
import * as schemaBancoLista from '../../../../../assets/schemas/banco/BancoLista.json';
import * as schemaBancoGet from '../../../../../assets/schemas/banco/BancoGet.json';
import * as schemaBancoCreate from '../../../../../assets/schemas/banco/BancoCreate.json';
import * as schemaBancoUpdate from '../../../../../assets/schemas/banco/BancoUpdate.json';
import * as schemaBancoDelete from '../../../../../assets/schemas/banco/BancoDelete.json';

@Component({
	selector: 'banco',
	templateUrl: './banco.component.html',
	styleUrls: ['./banco.component.scss']
})
export class BancoComponent implements OnInit {
	jsonBancoLista: object = schemaBancoLista;
	jsonBancoGet: object = schemaBancoGet;
	jsonBancoCreate: object = schemaBancoCreate;
	jsonBancoUpdate: object = schemaBancoUpdate;
	jsonBancoDelete: object = schemaBancoDelete;

	public listForm: FormGroup;
	public getForm: FormGroup;
	public createForm: FormGroup;
	public updateForm: FormGroup;
	public deleteForm: FormGroup;

	public paramsId;
	public paramsOption;
		// list
		// details
		// update
		// delete
		// create

	constructor(private ProvRequest: ProvService, private rutaActiva: ActivatedRoute, private router: Router) { }

	ngOnInit() {
		this.rutaActiva.params.subscribe(
			(params: Params) => {
			  this.paramsId = params.id;
			  this.paramsOption = params.option;
			}
		  );
		console.log('Banco');
		console.log(this.paramsId);
		console.log(this.paramsOption);
		if (this.paramsOption !== 'list' && this.paramsOption !== 'details' && this.paramsOption !== 'update' && this.paramsOption !== 'create' && this.paramsOption !== 'delete') {
			this.router.navigate(['/bank/list']);
			this.paramsOption = 'list';
		}
		this.listForm = new FormGroup({
			activo: new FormControl(''),
			page: new FormControl(''),
			pageSize: new FormControl(''),
			name: new FormControl('', [Validators.maxLength(60)])
		});
		this.getForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
		this.createForm = new FormGroup({
			nombre: new FormControl('', [Validators.required]),
			activo: new FormControl(true, [Validators.required])
		});
		this.updateForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
			nombre: new FormControl('', [Validators.required]),
			activo: new FormControl('', [Validators.required])
		});
		this.deleteForm = new FormGroup({
			id: new FormControl('', [Validators.required]),
		});
	}

	public onSubmitGet(formPath,formValue,boxResponse) {
		this.ProvRequest.submitGet(formPath,formValue.value,boxResponse);
	}

	public onSubmitPost(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPost(formPath,formValue.value,boxResponse);
	}

	public onSubmitPut(formPath,formValue,boxResponse) {
		this.ProvRequest.submitPut(formPath,formValue.value,boxResponse);
	}

	public onSubmitDelete(formPath,boxResponse) {
		this.ProvRequest.submitDelete(formPath,boxResponse);
	}
}
