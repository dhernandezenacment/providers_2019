// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../../core/auth';

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dashboard',
				loadChildren: 'app/views/pages/dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'bank',
				loadChildren: 'app/views/pages/proveedores/banco/banco.module#BancoModule'
			},
			{
				path: 'account',
				loadChildren: 'app/views/pages/proveedores/cuenta/cuenta.module#CuentaModule'
			},
			{
				path: 'position',
				loadChildren: 'app/views/pages/proveedores/geografia/geografia.module#GeografiaModule'
			},
			{
				path: 'organization',
				loadChildren: 'app/views/pages/proveedores/organizacion/organizacion.module#OrganizacionModule'
			},
			{
				path: 'organization-type',
				loadChildren: 'app/views/pages/proveedores/organizacionTipo/organizacionTipo.module#OrganizacionTipoModule'
			},
			{
				path: 'service',
				loadChildren: 'app/views/pages/proveedores/servicio/servicio.module#ServicioModule'
			},
			{
				path: 'service-type',
				loadChildren: 'app/views/pages/proveedores/servicioTipo/servicioTipo.module#ServicioTipoModule'
			},
			{
				path: 'builder',
				loadChildren: 'app/views/themes/demo5/content/builder/builder.module#BuilderModule'
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
