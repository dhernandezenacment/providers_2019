export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Banco',
					root: true,
					alignment: 'left',
					toggle: 'click',
					submenu: [
						{
							title: 'Lista',
							page: '/bank/list'
						},
						{
							title: 'Crear',
							page: '/bank/create'
						},
						{
							title: 'Detalles',
							page: '/bank/details'
						},
						{
							title: 'Actualizar',
							page: '/bank/update'
						},
						{
							title: 'Eliminar',
							page: '/bank/delete'
						},
					]
				},
				{
					title: 'Cuenta',
					root: true,
					alignment: 'left',
					page: '/account',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Geografía',
					root: true,
					alignment: 'left',
					page: '/position',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Dirección',
					root: true,
					alignment: 'left',
					// page: '/direccion',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Servicio',
					root: true,
					alignment: 'left',
					page: '/service',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Organización',
					root: true,
					alignment: 'left',
					// page: '/organization',
					toggle: 'click',
					submenu: [
						{
							title: 'Lista',
							page: '/organization/list'
						},
						{
							title: 'Crear',
							page: '/organization/create'
						},
						{
							title: 'Detalles',
							page: '/organization/details'
						},
						{
							title: 'Actualizar',
							page: '/organization/update'
						},
						{
							title: 'Eliminar',
							page: '/organization/delete'
						},
					]
				},
				{
					title: 'Tipo de Organización',
					root: true,
					alignment: 'left',
					page: '/organization-type',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Tipo de Servicio',
					root: true,
					alignment: 'left',
					page: '/service-type',
					toggle: 'click',
					// translate: 'MENU.DASHBOARD',
				},
				// {
				// 	title: 'Components',
				// 	root: true,
				// 	alignment: 'left',
				// 	toggle: 'click',
				// 	// submenu: []
				// },
				// {
				// 	title: 'Applications',
				// 	root: true,
				// 	alignment: 'left',
				// 	toggle: 'click',
				// 	// submenu: []
				// },
				// {
				// 	title: 'Custom',
				// 	root: true,
				// 	alignment: 'left',
				// 	toggle: 'click',
				// 	// submenu: []
				// },
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'line',
				},
				{
					title: 'Layout Builder',
					root: true,
					icon: 'flaticon2-expand',
					page: '/builder'
				},
				{section: 'Custom'},
				{
					title: 'Custom Link',
					root: true,
					icon: 'flaticon2-link',
					bullet: 'line',
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
