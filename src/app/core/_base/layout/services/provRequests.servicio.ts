// Angular
import { Injectable } from '@angular/core';
// RxJS
// import { Observable } from 'rxjs';

import axios from 'axios';

@Injectable()
export class ProvService {
	private static readonly BASE_URL = 'https://dev.tmapi.tech/proveedores';
	constructor() {
		axios.defaults.baseURL = ProvService.BASE_URL;
		axios.defaults.headers.common['Content-type'] = 'application/json';
		/* axios.defaults.auth = {
			username: privateKey,
			password: ''
		}; */
		axios.defaults.responseType = 'json';
		axios.defaults.timeout = 90000;

		// Valor global para los post's / put's
		axios.defaults.headers.post['Content-Type'] = 'application/json';
		axios.defaults.headers.put['Content-Type'] = 'application/json';
	}

	async get(path) {
		return await axios.get(path);
	}

	async post(path, data) {
		return await axios.post(path, data);
	}

	async put(path, data) {
		return await axios.put(path, data);
	}

	async delete(path) {
		return await axios.delete(path);
	}

	submitGet(path: string, data: any, boxResponse: string) {
		let path2 = path;
		let count = 0;
		for (const key in data) {
		  	if (data.hasOwnProperty(key) && data[key] !== '' && data[key] !== null) {
				if (count == 0) {
					path2 = path2 + '?';
				} else {
					path2 = path2 + '&';
				}
				path2 = path2 + key + '=' + encodeURI(data[key]);
				count++;
			}
		}
		this.get(path2)
			.then(response => {
				console.log(response);
				let tmp_response = '';
				if (response.data.totalCount !== undefined) {
					for (const i in response.data.body) {
						tmp_response += JSON.stringify(response.data.body[i]) + '<br>';
					}
				} else {
					// Condicional cuando solo hay un elemento en la respuesta
					tmp_response += JSON.stringify(response.data.body);
				}
				document.getElementById(boxResponse).innerHTML = tmp_response;
			})
			.catch(error => {
				console.error('Error: ' + error);
				document.getElementById(boxResponse).innerHTML = 'No se encontraron registros';
			});
	}

	submitPost(path: string, data: any, boxResponse: string) {
		this.post(path, data)
		  	.then(response => {
				console.log(response);
				let tmp_response = '';
				if (response.data.totalCount !== undefined) {
					for (const i in response.data.body) {
						tmp_response += JSON.stringify(response.data.body[i]) + '<br>';
					}
				} else {
					// Condicional cuando solo hay un elemento en la respuesta
					tmp_response += JSON.stringify(response.data.body);
				}
				document.getElementById(boxResponse).innerHTML = tmp_response;
		  	})
		  	.catch(error => {
				console.error('Error: ' + error);
				document.getElementById(boxResponse).innerHTML = 'No se encontraron registros';
		  	});
	}

	submitPut(path: string, data: any, boxResponse: string) {
		this.put(path, data)
		  	.then(response => {
				// console.log(response.data.totalCount);
				console.log(response);
				let tmp_response = '';
				if (response.data.totalCount !== undefined) {
					for (const i in response.data.body) {
						tmp_response += JSON.stringify(response.data.body[i]) + '<br>';
					}
				} else {
					// Condicional cuando solo hay un elemento en la respuesta
					tmp_response += JSON.stringify(response.data.body);
				}
				document.getElementById(boxResponse).innerHTML = tmp_response;
			})
			.catch(error => {
				console.error('Error: ' + error);
				document.getElementById(boxResponse).innerHTML = 'No se encontraron registros';
			});
	}

	submitDelete(path: string, boxResponse: string) {
		this.delete(path)
			.then(response => {
				console.log(response);
				let tmp_response = '';
				if (response.status === 204) {
					// tmp_response += JSON.stringify(response.status) + '<br>';
					tmp_response += 'Banco eliminado';
				} else {
					tmp_response += JSON.stringify(response.status);
				}
				document.getElementById(boxResponse).innerHTML = tmp_response;
			})
			.catch(error => {
				console.error('Error: ' + error);
				document.getElementById(boxResponse).innerHTML = 'No se encontraron registros';
			});
	}
}
